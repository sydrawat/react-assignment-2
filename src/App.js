import React, { Component } from 'react';
import ValidationComponent from './ValidationComponent/ValidationComponent'
import './App.css';
import CharComponent from './CharComponent/CharComponent';

class App extends Component {
  state ={
    text : '',
    textLength : 0
  }

  inputChangeHandler = (event) => {
    const text = event.target.value;
    const textLength = text.toString().length;

    this.setState({
      text: text,
      textLength: textLength
    });

  }

  render() {
    return (
      <div className="App">
        <h1>React App working!</h1>
        <input type="text" 
          onChange={this.inputChangeHandler}/>
        <p>The length of input value is : {this.state.textLength}</p>
        <ValidationComponent 
          text={this.state.text} />
        <CharComponent />
      </div>
    );
  }
}

export default App;
