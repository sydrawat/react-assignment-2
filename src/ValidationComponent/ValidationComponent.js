import React from 'react';
import './ValidationComponent.css'

const ValidationConponent = (props) => {
    const {text} = props;
    let message = 'too short!';
    if (text.trim().length >= 5) {
        message = 'long enough!';
    }
    return(
        <div className="ValidationComponent">
            <p>The text is : {message}</p>
        </div>
    );
}

export default ValidationConponent;